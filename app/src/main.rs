use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;

use log;
use structopt::StructOpt;

use models::Arguments;

mod models;
mod services;

fn main() -> Result<(), Box<dyn Error>> {
    simple_logger::init().unwrap();
    let _args: Arguments = Arguments::from_args();
    let _path: &str = _args.path.to_str().unwrap_or_default();
    let _pattern = &_args.pattern;

    return run(_pattern, _path);
}

fn run(pattern: &str, path: &str) -> Result<(), Box<dyn Error>> {
    let _file: File = File::open(path)?;

    let _reader = BufReader::new(_file);

    let _matches: Vec<(usize, String)> = services::search(pattern, _reader.lines(), predicate);
    log::info!("Found {} matches", _matches.len());

    return Ok(());
}

fn predicate(pattern: &str, content: &str) -> bool {
    return content.contains(pattern);
}
