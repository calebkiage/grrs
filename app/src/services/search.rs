use std::fs::File;
use std::io::{BufReader, Lines};

pub fn search(pattern: &str, lines: Lines<BufReader<File>>, predicate: fn(&str, &str) -> bool) -> Vec<(usize, String)> {
    let mut _count: usize = 0;
    let result = lines.map(|line| {
        _count += 1;
        let _content: String = line.unwrap_or_default();
        return (_count, _content)
    }).filter(|tuple| {
        return predicate(pattern, tuple.1.as_str());
    }).collect();
    return result;
}
